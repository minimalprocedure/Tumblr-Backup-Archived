/*******************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: minimal.procedure@gmail.com
 * web: http://pragmas.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.pragmas.tumblr

import java.io._

class FileHelper(file: File) {

  def writeln(text: String, fw : FileWriter = null): FileWriter = {
    var fileWriter: FileWriter = null
    if(fw != null)
      fileWriter = fw
    else
      fileWriter = new FileWriter(file)
    try { fileWriter.write(text + "\n") } catch { case e: Exception => fileWriter.close }
    fileWriter
  }

  def write(text: String): Unit = {
    val fw = new FileWriter(file)
    try { fw.write(text) } finally { fw.close }
  }

  def foreachLine(proc: String => Unit): Unit = {
    val br = new BufferedReader(new FileReader(file))
    try { while (br.ready) proc(br.readLine) } finally { br.close }
  }

  def deleteAll: Unit = {
    def deleteFile(dfile: File): Unit = {
      if (dfile.isDirectory) {
        val subfiles = dfile.listFiles
        if (subfiles != null)
          subfiles.foreach{ f => deleteFile(f) }
      }
      dfile.delete
    }
    deleteFile(file)
  }
}

object FileHelper {
  implicit def file2helper(file: File) = new FileHelper(file)
}

/*
//Now you can test it.
import FileHelper._
val dir = new File("/tmp/mydir/nested_dir")
dir.mkdirs
val file = new File(dir, "myfile.txt")
file.write("one\ntwo\nthree")
file.foreachLine{ line => println(">> " + line) }
dir.deleteAll
*/
