/**
 * *****************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: minimal.procedure@gmail.com
 * web: http://pragmas.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
package org.pragmas.tumblr

import java.net.{ URLConnection, URL, HttpURLConnection }
import java.io._
import java.net.{ URI, URLEncoder }
import scala.xml._
import scala.collection.mutable.{ Queue, HashMap, ArrayBuffer, ListBuffer, StringBuilder }
import scala.io.BufferedSource

import FileHelper._

object TumblrBackup {

  val MAX_NUM = 50

  val VERSION_MAJOR = 1
  val VERSION_MINOR = 0
  val VERSION_SUBMINOR = 0
  val VERSION_MARKER = "beta11"
  val COPYRIGHT = "(c) Massimo Maria Ghisalberti 2010-2011\n<zairik@gmail.com> http://pragmas.org"

  def VERSION: String = {
    VERSION_MAJOR.toString + "." +
      VERSION_MINOR.toString + "." +
      VERSION_SUBMINOR.toString + "." +
      VERSION_MARKER
  }

  var engine: TumblrBackup = null

  def postNumber(username: String): Int = {
    engine = new TumblrBackup(username)
    engine.totalPostsNumber(engine.getPosts(0, 1))
  }

  def backupPublic(username: String,
                   base: String,
                   verbose: Boolean = true): Unit = {
    if (engine == null) {
      engine = new TumblrBackup(username)
      engine.LOGGER.VERBOSE = verbose
      engine.backupPublic(base, 0, 0, MAX_NUM)
    } else engine = null
  }

  def backupPublicByNum(username: String,
                        base: String,
                        from: Int = 0,
                        to: Int = 0,
                        num: Int = MAX_NUM,
                        verbose: Boolean = true): Unit = {
    if (engine == null) {
      engine = new TumblrBackup(username)
      engine.LOGGER.VERBOSE = verbose
      engine.backupPublic(base, from, to, num)
    } else engine = null
  }
}

class TumblrBackup(userId: String) {

  private var baseDir: File = _
  private var rootUrl: String = _
  private var currentDir: File = _
  private var currentNumPost: Int = 0

  val LOGGER = Logger

  private var postLocalLinks = ListBuffer[String]()

  /*
  def backupPublicOld(base: String, startPost: Int = 0, numPosts: Int = TumblrBackup.MAX_NUM): Unit = {
    LOGGER.message("start...")
    val total = totalPostsNumber(getPosts(0, 1))
    val num: Int = if (numPosts <= TumblrBackup.MAX_NUM && numPosts > 1) numPosts else 50
    val start: Int = if (startPost < total && startPost >= 0) startPost else 0
    makeBaseDir(base)
    var posts = getPosts(start, num)
    storeMeta(baseDir, getData(posts))
    filterPosts(posts)
    makeContentRootFiles
    LOGGER.message("done!")
  }
 */

  def backupPublic(base: String, from: Int = 0, to: Int = 0, num: Int = TumblrBackup.MAX_NUM): Unit = {

    LOGGER.message("START BACKUP PROCESS...")
    makeBaseDir(base)

    var start = from
    val total = if (to <= 0)
      totalPostsNumber(getPosts(0, 1))
    else to

    LOGGER.message(s"BACKUP FROM: $start TO: $total BULK: $num")
    var posts = getPosts(start, num)
    storeMeta(baseDir, getData(posts))
    filterPosts(posts)
    start += num

    val cicle = total / num
    val remainder = total % num

    for (index <- 1 until cicle) {
      LOGGER.message(s"BACKUP FROM: $start TO: $total BULK: $num")
      posts = getPosts(start, num)
      filterPosts(posts)
      start += num
    }

    if (remainder > 0) {
      LOGGER.message(s"BACKUP FROM: $start TO: $total BULK: $num")
      posts = getPosts(start, remainder)
      filterPosts(posts)
    }

    makeContentRootFiles
    LOGGER.message("END BACKUP PROCESS")

  }

  //private def encode(str : String) : String = URLEncoder.encode(str, "UTF-8")

  private def makeContentRootFiles: Unit = {
    val file = new File(baseDir, "stylesheet.css")
    file.write("/* STYLESHEET */")
    storeContent(baseDir, rootUrl, makeMainContent, null, null, "./")
  }

  private def makeBaseDir(base: String): Unit = {
    rootUrl = userId + ".tumblr.com"
    baseDir = new File(base + "/" + rootUrl)
    baseDir.mkdirs
  }

  private def getPosts(start: Int = 0, num: Int = 2): Elem = {
    val url = new URL("http://" + userId + ".tumblr.com/api/read?start=" + start.toString + "&num=" + num.toString)
    XML.load(url)
  }

  private def getData(root: Elem): Map[String, String] = {
    val tumbleLog = root \ "tumblelog"
    Map[String, String](
      "name" -> (tumbleLog \ "@name").text,
      "timezone" -> (tumbleLog \ "@timezone").text,
      "title" -> (tumbleLog \ "@title").text,
      "content" -> (tumbleLog).text)
  }

  def totalPostsNumber(root: Elem): Int = {
    val posts = root \ "posts"
    (posts \ "@total").text.toInt
  }

  private def filterPosts(root: Elem): Unit = {
    val posts = root \ "posts"
    posts \ "post" foreach { (post) =>
      currentNumPost += 1
      LOGGER.message(currentNumPost, "PROCESS POST NUM")
      processPost(post)
    }
  }

  private def processPost(post: Node): Unit = {
    val metadata = metadataPost(post)
    metadata("type") match {
      case TumblrTags.REGULAR => if (TumblrTags.isFilterActive(TumblrTags.REGULAR)) processRegular(post, metadata)
      case TumblrTags.PHOTO => if (TumblrTags.isFilterActive(TumblrTags.PHOTO)) processPhoto(post, metadata)
      case TumblrTags.QUOTE => if (TumblrTags.isFilterActive(TumblrTags.QUOTE)) processQuote(post, metadata)
      case TumblrTags.LINK => if (TumblrTags.isFilterActive(TumblrTags.LINK)) processLink(post, metadata)
      case TumblrTags.CONVERSATION => if (TumblrTags.isFilterActive(TumblrTags.CONVERSATION)) processConversation(post, metadata)
      case TumblrTags.VIDEO => if (TumblrTags.isFilterActive(TumblrTags.VIDEO)) processVideo(post, metadata)
      case TumblrTags.AUDIO => if (TumblrTags.isFilterActive(TumblrTags.AUDIO)) processAudio(post, metadata)
      case TumblrTags.ANSWER => if (TumblrTags.isFilterActive(TumblrTags.ANSWER)) processAnswer(post, metadata)
      case _ => processUnknown(post, metadata)
    }
  }

  private def accumulateTags(node: NodeSeq): List[String] = {
    val tags = new ListBuffer[String]()
    node foreach { (tag) => tags += tag.text }
    tags.toList
  }

  private def metadataPost(node: Node): Map[String, String] = {
    Map[String, String](
      "id" -> (node \ "@id").text,
      "url" -> (node \ "@url").text,
      "url-with-slug" -> (node \ "@url-with-slug").text,
      "type" -> (node \ "@type").text,
      "date-gmt" -> (node \ "@date-gmt").text,
      "date" -> (node \ "@date").text,
      "unix-timestamp" -> (node \ "@unix-timestamp").text,
      "format" -> (node \ "@format").text,
      "reblog-key" -> (node \ "@reblog-key").text,
      "slug" -> (node \ "@slug").text,
      "mobile" -> (node \ "@mobile").text,

      "bookmarklet" -> (node \ "@bookmarklet").text,

      "width" -> (node \ "@width").text,
      "height" -> (node \ "@height").text)
  }

  private def prepareFolder(meta: Map[String, String]): File = {
    val folder = "(" + meta("date-gmt").replace(" ", "_").replace(":", "-") + ")_(" + meta("slug") + ")_(" + meta("type") + ")"
    postLocalLinks += folder

    LOGGER.message(folder, "PREPARE FOLDER")

    val storeDir = new File(this.baseDir.getAbsolutePath + "/" + folder)
    storeDir.mkdirs
    storeDir
  }

  private def makeMainContent: String = {
    var linksList =
      <ul id="postLocalLinks">
        {
          for (link <- postLocalLinks)
            yield <li>
                    <dl id={ link }>
                      <dt>{ link }</dt>
                      <dd><a href={ "./" + link + "/index.html" }>content</a></dd>
                      <dd><a href={ "./" + link + "/meta.txt" }>meta</a></dd>
                      <dd><a href={ "./" + link + "/tags.txt" }>tags</a></dd>
                    </dl>
                  </li>
        }
      </ul>
    linksList.toString
  }

  private def storeMeta(storeDir: File, meta: Map[String, String]): Unit = {

    LOGGER.message("meta.txt", "STORE METAS")

    val fileMeta = new File(storeDir, "meta.txt")
    val wMeta = fileMeta.writeln("--")
    meta foreach { (m) => fileMeta.writeln(m._1 + ":" + m._2, wMeta) }
    wMeta.close
  }

  private def storeTags(storeDir: File, tags: List[String]): Unit = {

    LOGGER.message("tags.txt", "STORE TAGS")

    val fileTags = new File(storeDir, "tags.txt")
    var tagsString = ""
    for (tag <- tags)
      tagsString += tag + "\n"
    fileTags.write(tagsString)
  }

  private def downloadFile(name: String, urlStr: String, extension: String = null): String = {

    LOGGER.message(urlStr, "DOWNLOAD FILE")

    val url = new URL(urlStr)
    val conn = url.openConnection.asInstanceOf[HttpURLConnection]
    conn.setInstanceFollowRedirects(true)
    conn.setConnectTimeout(15000)
    conn.setReadTimeout(15000)
    conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; U; Linux i686; it; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12 FirePHP/0.5")
    conn.connect()
    val ctype = conn.getContentType
    var ext = if (extension == null) "." + ctype.substring(ctype.lastIndexOf("/") + 1) else "." + extension
    val filename = name + ext

    //    println("------------------------------------------")
    //    println(conn.getInstanceFollowRedirects)
    //    println("------------------------------------------")

    val in = new BufferedInputStream(conn.getInputStream)
    val fos = new java.io.FileOutputStream(filename)
    val bout = new BufferedOutputStream(fos, 1024)
    val data = new Array[Byte](1024)
    var x = 0
    x = in.read(data, 0, 1024)
    while (x >= 0) {
      bout.write(data, 0, x)
      x = in.read(data, 0, 1024)
    }
    bout.close
    in.close
    conn.disconnect
    filename
  }

  private def storeContent(
    storeDir: File,
    title: String,
    content1: String,
    content2: String,
    content3: String,
    base: String = "../"): Unit = {
    var stylepath = base + "stylesheet.css"
    var html =
      <html lang="it" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="content-Type" content="text/html; charset=UTF-8"/>
          <link xmlns="" type="text/css" rel="stylesheet" href={ stylepath }/>
          <title>{ Unparsed(title) }</title>
        </head>
        <body>
          <h1 id="contentTitle">{ Unparsed(title) }</h1>
          { if (content1 != null) <div id="content1Body">{ Unparsed(content1) }</div> }
          { if (content2 != null) <div id="content2Body">{ Unparsed(content2) }</div> }
          { if (content3 != null) <div id="content3Body">{ Unparsed(content3) }</div> }
        </body>
      </html>
    val file = new File(storeDir, "index.html")
    file.write(html.toString)
  }

  private def prepareMainData(node: Node): Map[String, String] = {
    val meta = metadataPost(node)
    var tags = accumulateTags(node \ "tag")
    currentDir = prepareFolder(meta)
    storeMeta(currentDir, meta)
    storeTags(currentDir, tags)
    meta
  }

  private def processRegular(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Regular post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val regularTitle = (node \ "regular-title").text
    val regularBody = (node \ "regular-body").text
    storeContent(currentDir, regularTitle, regularBody, null, null)
    LOGGER.message("Regular post - done.", "DOWNLOAD")
  }

  private def processPhoto(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Photo post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val photoCaption = (node \ "photo-caption").text
    val photoUrls = node \ "photo-url"
    var photosUrls = ListBuffer[String]()
    var fileName = ""
    var fileExt = ""

    for (photo <- photoUrls; val width = (photo \ "@max-width").text; if TumblrTags.isSizeActive(width) == true) {
      //val width = (photo \ "@max-width").text
      val urlString = photo.text
      val photoName = meta("slug") + "-" + width
      photosUrls += photoName
      val photoDir = new File(currentDir + "/" + photoName)
      fileName = downloadFile(photoDir.getAbsolutePath, urlString)
      fileExt = fileName.substring(fileName.lastIndexOf(".") + 1)
    }

    var photoBlock = <ul>
                       { for (photoUrl <- photosUrls) yield <li><img src={ "./" + photoUrl + "." + fileExt } alt={ photoUrl }/></li> }
                     </ul>
    storeContent(currentDir, photoCaption, photoBlock.toString, null, null)
    LOGGER.message("Photo post - done.", "DOWNLOAD")
  }

  private def processQuote(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Quote post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val quoteText = (node \ "quote-text").text
    val quoteSource = (node \ "quote-source").text
    storeContent(currentDir, "Quote", quoteText, quoteSource, null, null)
    LOGGER.message("Quote post - done.", "DOWNLOAD")
  }

  private def processLink(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Link post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val linkText = (node \ "link-text").text
    val linkUrl = (node \ "link-url").text
    val linkDescription = (node \ "link-description").text
    storeContent(currentDir, "Link", linkText, "<a href=\"" + linkUrl + "\">" + linkUrl + "</a>", linkDescription)
    LOGGER.message("Link post - done.", "DOWNLOAD")
  }

  private def processConversation(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Conversation post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val conversationTitle = (node \ "conversation-title").text
    //val conversationText = (node \ "conversation-text").text
    val lines = node \ "conversation" \ "line"

    var conversation = <ul>{ for (line <- lines) yield <li>{ (line \ "@label").text }{ line.text }</li> }</ul>

    storeContent(currentDir, conversationTitle, conversation.toString, null, null)
    LOGGER.message("Conversation post - done.", "DOWNLOAD")
  }

  private def processVideo(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Video post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val videoCaption = (node \ "video-caption").text
    val videoSource = (node \ "video-source").text
    val videoPlayer = (node \ "video-player").text
    storeContent(currentDir, videoCaption, videoSource, videoPlayer, null)
    LOGGER.message("Video post - done.", "DOWNLOAD")
  }

  private def processAudio(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Audio post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val audioCaption = (node \ "audio-caption").text
    val audioPlayer = (node \ "audio-player").text

    val id3 = <ul>
                <li>{ (node \ "id3-artist").text }</li>
                <li>{ (node \ "id3-album").text }</li>
                <li>{ (node \ "id3-year").text }</li>
                <li>{ (node \ "id3-track").text }</li>
                <li>{ (node \ "id3-title").text }</li>
              </ul>

    storeContent(currentDir, audioCaption, audioPlayer, id3.toString, null)
    LOGGER.message("Audio post - done.", "DOWNLOAD")
  }

  private def processAnswer(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.message("Answer post - start...", "DOWNLOAD")
    val meta = prepareMainData(node)
    val question = (node \ "question").text
    val answer = (node \ "answer").text
    storeContent(currentDir, "Question", question, answer, null)
    LOGGER.message("Answer post - done.", "DOWNLOAD")
  }

  private def processUnknown(node: Node, metadata: Map[String, String]): Unit = {
    LOGGER.warn(node, "UNKNOWN NODE")
  }

}
