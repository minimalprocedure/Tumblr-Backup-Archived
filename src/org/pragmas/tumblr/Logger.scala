/*******************************************************************************
 * Copyright 2010 Massimo Maria Ghisalberti {nissl:asbru}
 * email: minimal.procedure@gmail.com
 * web: http://pragmas.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.pragmas.tumblr

import org.eclipse.swt.widgets.Text
import org.eclipse.swt.widgets.Display

trait ILogger {

  var GUILOGGERVIEWER : Text = _

  private def printMessageLn(message : String) : Unit = {
    if (GUILOGGERVIEWER != null) {
      Display.getDefault.syncExec(
        new Runnable() {
          override def run : Unit = {
            GUILOGGERVIEWER.append(message + "\n")
          }
        })
    } else println(message)
  }

  private def printMessage(message : String) : Unit = {
    if (GUILOGGERVIEWER != null) {
      Display.getDefault.syncExec(
        new Runnable() {
          override def run : Unit = {
            GUILOGGERVIEWER.append(message)
          }
        })
    } else print(message)
  }

  var VERBOSE = true

  def out(data : Any, msgPrefix : String = null) : Unit = {
    val prefix = if (msgPrefix != null) msgPrefix + ":" else ""
    printMessageLn(prefix + data.toString)
  }

  def message(data : Any, msgPrefix : String = null) : Unit = {
    if (VERBOSE) {
      val prefix = if (msgPrefix != null) msgPrefix + ":" else ""
      val message = "MESSAGE:" + prefix + data.toString
      printMessageLn(message)
    } else printMessage(".")
  }

  def log(data : Any, msgPrefix : String = null) : Unit = {
    val prefix = if (msgPrefix != null) msgPrefix + ":" else ""
    val message = "LOG:" + prefix + data.toString
    printMessageLn(message)
  }

  def error(data : Any, msgPrefix : String = null) : Unit = {
    val prefix = if (msgPrefix != null) msgPrefix + ":" else ""
    val message = "ERROR:" + prefix + data.toString
    printMessageLn(message)
  }

  def warn(data : Any, msgPrefix : String = null) : Unit = {
    val prefix = if (msgPrefix != null) msgPrefix + ":" else ""
    val message = "WARNING:" + prefix + data.toString
    printMessageLn(message)
  }
}

object Logger extends ILogger

